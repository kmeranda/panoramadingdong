"use strict";

$(function(){
    var usersRef = new Firebase('https://panorama-dd.firebaseio.com/users');
    var imagesRef = new Firebase('https://panorama-dd.firebaseio.com/images/' + panoid);
    var firebase = new Firebase('https://panorama-dd.firebaseio.com/');
    var likeRef, favRef;
    var globalAuthData = undefined;

    firebase.onAuth(function(authData) {
        globalAuthData = authData;
        if (authData) {
            $('#form-comment').show();
            $('#login-prompt').hide();

            likeRef = usersRef.child(authData.uid).child('liked');
            favRef = usersRef.child(authData.uid).child('favorites');
        } else {
            $('#form-comment').hide();
            $('#login-prompt').show();
            likeRef = favRef = undefined;
        }
    });

    var showError = function(title, text) {
        var $error = $('.error');
        $error
            .html('<h3>' + title + '</h3><p>' + text + '</p>')
            .slideDown();
        setTimeout(function() {
            $error.slideUp('fast');
        }, 5000);
    };

    var printUsername = function(uid, element) {
        usersRef.child(uid).once('value', function(snapshot) {
            element.html('<a href="/users/'+ uid + '">' + snapshot.val().name + '</a>');
        });
    };

    var loaded = false;
    imagesRef.on('value', function(snapshot) {
        var info = snapshot.val();
        usersRef.child(info.creator).once('value', function(userSnapshot) {
            $('.photo-title').html('<h1>' + info.title + '</h1>');
            $('.photo-creator').html('by <b><a href="/users/'+ info.creator + '">' + (userSnapshot.val().name || 'Unnamed User') + '</a></b>');
            $('.photo-description').html(info.description || '');
            if (info.date) {
                var date = new Date(info.date.replace('-','/'));
                $('.photo-taken').html('<b>Taken: </b>' + date.toDateString());
            }
            if (info.tags) {
                var tagString = 'Tags: ';
                for (var tag in info.tags) {
                    if (info.tags.hasOwnProperty(tag)) {
                        tagString = tagString + info.tags[tag] + ', ';
                    }
                }
                // trim the trailing comma-space
                tagString = tagString.substring(0, tagString.length - 2);
                $('.photo-tags').html(tagString);
            }
            if (info.comments) {
                var $commentBlock = $('#comments-wrapper').html('<hr />');
                var numComments = 0;
                for (var c in info.comments) {
                    if (info.comments.hasOwnProperty(c)) {
                        var comment = info.comments[c];
                        var $newComment = $('<div id="comment-'+c+'">' +
                            '<h3>User</h3><p>'+comment.text+'</p></div><hr />');
                        $commentBlock.append($newComment);
                        printUsername(comment.uid, $newComment.children('h3'));
                        numComments++;
                    }
                }
                $('#comment-header').html('Comments ('+numComments+')');
            }
            $('#like-count').html(info.likes || 0);
            $('#fav-count').html(info.favorites || 0);

            if (info.userLikes && globalAuthData && info.userLikes[globalAuthData.uid]) {
                $('#like-txt').html('Liked!');
                $('.photo-likes').addClass('photo-liked');
            } else {
                $('#like-txt').html('Like');
                $('.photo-likes').removeClass('photo-liked');
            }
            if (info.userFavorites && globalAuthData && info.userFavorites[globalAuthData.uid]) {
                $('#fav-txt').html('Favorited!');
                $('.photo-favorites').addClass('photo-favorited');
            } else {
                $('#fav-txt').html('Favorite');
                $('.photo-favorites').removeClass('photo-favorited');
            }

            // if we already loaded the viewer, don't load it again
            if (loaded) {
                return;
            }

            OpenSeadragon({
                element: $('#photo-section')[0],
                visibilityRatio: 1.0,
                tileSources: info['aws-url']
            });
            loaded = true;
        });
    })

    $('#form-comment').on('submit', function(evt) {
        evt.preventDefault();

        var auth = firebase.getAuth();
        var $commentText = $('#inp-comment');
        if ($commentText.val() === '') {
            return;
        }

        if (!auth) {
            $('#form-comment').hide();
            $('#login-prompt').show();
            return;
        }

        imagesRef.child('comments').push({uid: auth.uid, text: $commentText.val()})
        $commentText.val('');
    });

    $('.photo-likes').on('click', function(evt) {
        evt.stopPropagation();

        var authData = firebase.getAuth(); // make sure we have the latest login data
        if (!authData) {
            return;
        }

        var notPrevLiked = true;
        likeRef.child(panoid).once('value').then(function(userLikeSnap) {
            if (userLikeSnap.exists()) {
                notPrevLiked = null;
            }

            return likeRef.child(panoid).set(notPrevLiked);
        }).then(function(imgLikeSnap) {
            return imagesRef.child('likes').transaction(function(old) {
                if (notPrevLiked === null) {
                    return (old || 1) - 1;
                } else {
                    return (old || 0) + 1;
                }
            });
        }).then(function(userLikeSnap) {
            return imagesRef.child('userLikes').child(authData.uid).set(notPrevLiked);
        });
    });
    $('.photo-favorites').on('click', function(evt) {
        evt.stopPropagation();

        var authData = firebase.getAuth(); // make sure we have the latest login data
        if (!authData) {
            return;
        }

        var notPrevFav = true;
        favRef.child(panoid).once('value').then(function(userFavSnap) {
            if (userFavSnap.exists()) {
                notPrevFav = null;
            }

            return favRef.child(panoid).set(notPrevFav);
        }).then(function(imgFavSnap) {
            return imagesRef.child('favorites').transaction(function(old) {
                if (notPrevFav === null) {
                    return (old || 1) - 1;
                } else {
                    return (old || 0) + 1;
                }
            });
        }).then(function(userFavSnap) {
            return imagesRef.child('userFavorites').child(authData.uid).set(notPrevFav);
        });
    });
});
