$(function() {
    var thumbsRef = new Firebase('https://panorama-dd.firebaseio.com/images').limitToLast(25);
    thumbs(thumbsRef, $('#thumbs-list'));
});
