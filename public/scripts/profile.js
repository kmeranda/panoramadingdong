"use strict";
/**
 * Created by Owner on 5/1/2016.
 */


$(function () {
    console.log(userid);

    var firebase = new Firebase("https://panorama-dd.firebaseio.com");
    this.firebase = firebase;
    var imageRef = this.firebase.child('images');
    var userRef = this.firebase.child('users/').child(userid);
    userRef.once("value", function(snapshot) {
        var $account = snapshot.val();
        $("#name").append($account.name);
        $("#username").append($account.username);
        $("#email").append($account.email);
        var $uploads = [];
        for (var key in $account.gallery) {
            $uploads.push($account.gallery[key]);
        }
        thumbs(imageRef, $("#uploads-list"), $uploads);
        var $favorites = [];
        for (var key in $account.favorites) {
            $favorites.push(key);
        }
        thumbs(imageRef, $("#favorites-list"), $favorites);
    });
});