
$(function() {
    var searchRef = new Firebase('https://panorama-dd.firebaseio.com/search');
    var imageRef = new Firebase('https://panorama-dd.firebaseio.com/images');
    var resultRef = undefined;
    $('#form-search').on('submit', function(evt) {
        evt.preventDefault();
        var query = $('#inp-search-text').val();
        if (query === '') {
            return;
        }

        if (resultRef) {
            resultRef.remove();
            resultRef.off('value');
            resultRef = undefined;
        }

        var newSearch = searchRef.child('request').push(query);
        resultRef = searchRef.child('response/'+newSearch.key());

        resultRef.on('value', function (snapshot) {
            if (snapshot.val() === null) { return; }
            var $results = $('#results');
            var hits = snapshot.val().hits;
            if (hits.total === 0) {
                $results.html('<p>No results found</p>');
                return;
            }
            hits = hits.hits;
            var hitArr = [];
            for (var img in hits) {
                if (hits.hasOwnProperty(img)) {
                    hitArr.push(hits[img]._id);
                }
            }
            $results.html('');
            thumbs(imageRef, $results, hitArr);
        });
    });

    $(window).on('unload', function(evt) {
        if (resultRef) {
            resultRef.off('value');
            resultRef.remove();
            resultRef = undefined;
        }
    });
});
