"use strict";

function PDDaccount(fbname) {
    var firebase = new Firebase("https://"+ fbname + ".firebaseio.com");
    this.firebase = firebase;
    var usersRef = this.firebase.child('users');
    this.usersRef = usersRef;
    var uid;
    var instance = this;
    //overridable functions
    this.onLogin = function(user, uid) {};
    this.onLoginFailure = function() {};
    this.onLogout = function() {};
    this.onError = function(error) {};
    // long running firebase listener
    this.start = function() {
        firebase.onAuth(function (authResponse) {
            if (authResponse) {
                console.log("user is logged in");
                usersRef.child(authResponse.uid).once('value', function(snapshot) {
                    instance.user = snapshot.val();
                    instance.onLogin(instance.user, snapshot.key());
                });
            } else {
                console.log("user is logged out");
                instance.onLogout();
            }
        });
    };
    this.uid = function() {return uid;};
    // signup
    this.signup = function(email,name,alias,password,confirm) {
        if (password !== confirm) {
            instance.onError("Passwords did not match.");
            $('#signup-form').show();
        } else {
            this.firebase.createUser({
                email: email,
                password: password
            }, function (error, userData) {
                if (error) {
                    instance.onError("Error creating user" + error);
                    $('#signup-form').show();
                }
                else {
                    instance.userData = userData;
                    console.log("user data", userData);
                    usersRef.child(userData.uid).set({
                        email: email,
                        name: name,
                        username: alias
                    }, function (error) {
                        if (error) {
                            instance.onError(error);
                        }
                        else {
                            instance.login(email, password);
                        }
                    });
                }
            });
        }
    };
    // login with email and password
    this.login = function(email,password) {
        this.firebase.authWithPassword({
            email: email,
            password : password
        }, function(error, authData) {
            if (!error) {
                instance.auth = authData;
                console.log("uid:", authData.uid);
            } else {
                instance.onError("login failed! " + error);
                instance.onLoginFailure();
            }
        }, {
            remember : "default"
        });
    };
    // logout
    this.logout = function() {
        this.firebase.unauth();
        instance.auth=null;
    };
}

$(function() {
    var firebase = new Firebase("https://panorama-dd.firebaseio.com/");

    var $loginButton = $('#login-button');
    var $signupButton = $('#signup-button');
    var $logoutButton = $('#logout-button');
    var $signupForm = $('#signup-form');
    var $loginForm = $('#login-form');
    var $loginPrompt = $('#login-prompt');
    var $welcomeText = $('#welcome-text');
    var $photoDiv = $('#photos');
    var $returnButton = $('#return-button');

    var pdd = new PDDaccount("panorama-dd");

    pdd.onLogin = function(user, uid) {
        console.log("in onLogin!");
        $loginButton.hide();
        $signupButton.hide();
        $logoutButton.show();
        $signupForm.hide();
        $loginForm.hide();
        $loginPrompt.hide();
        $photoDiv.show();
        $returnButton.hide();
        $welcomeText.html('Welcome, <b><a href="/users/'+ uid + '">' + user.name + '</a></b>').show();
    };
    pdd.onLogout = function() {
        console.log("in onLogout");
        $loginButton.show();
        $signupButton.show();
        $logoutButton.hide();
        $loginForm.hide();
        $signupForm.hide();
        $loginPrompt.show();
        $photoDiv.hide();
        $returnButton.hide();
        $welcomeText.html('').hide();
    };
    pdd.onLoginFailure = function() {
        console.log("in onLoginFailure");
        $loginButton.show();
        $signupButton.show();
        $loginPrompt.show();
        $returnButton.hide();
    };
    $logoutButton.on('click',function(e) {
        pdd.logout();
        $logoutButton.hide();
        $loginButton.show();
        $signupButton.show();
        // Return to the homescreen
        window.location.href = "/";
        return false;
    });
    pdd.onError = function(error) {
        console.log("danger", error);
    };

    $loginButton.show();
    $signupButton.show();
    $logoutButton.hide();
    $signupForm.hide();
    $loginForm.hide();
    $loginPrompt.show();
    $photoDiv.hide();
    $returnButton.hide();

    // login forms
    $loginButton.on('click',function(e) {
        console.log("clicked login button!");
        $loginButton.hide();
        $signupButton.hide();
        $signupForm.hide();
        $loginForm.show();
        $returnButton.show();
        $('#login-email').val("").focus();
        $('#login-password').val("").blur();
        return false;
    });
    $loginForm.on('submit',function(e) {
        $loginForm.hide();
        e.preventDefault();
        e.stopPropagation();
        pdd.login($(this).find('#login-email').val(), $(this).find('#login-password').val());
        $('#login-email').val("").blur();
        $('#login-password').val("").blur();
        return false;
    });
    $signupButton.on('click',function(e) {
        console.log("clicked signup button");
        $signupButton.hide();
        $loginButton.hide();
        $loginForm.hide();
        $signupForm.show();
        $returnButton.show();
        $('#signup-email').val("").focus();
        $('#signup-name').val("").blur();
        $('#signup-alias').val("").blur();
        $('#signup-password').val("").blur();
        $('#signup-password-2').val("").blur();
        return false;
    });
    $signupForm.on('submit', function(e) {
        $signupForm.hide();
        e.preventDefault();
        e.stopPropagation();
        pdd.signup($(this).find('#signup-email').val(),
            $(this).find('#signup-name').val(),
            $(this).find('#signup-alias').val(),
            $(this).find('#signup-password').val(),
            $(this).find('#signup-password-2').val());
        $('#signup-email').val("").blur();
        $('#signup-name').val("").blur();
        $('#signup-alias').val("").blur();
        $('#signup-password').val("").blur();
        $('#signup-password-2').val("").blur();
    });
    $returnButton.on('click',function(e) {
        console.log("clicked return/back button!");
        $loginButton.show();
        $signupButton.show();
        $logoutButton.hide();
        $signupForm.hide();
        $loginForm.hide();
        $loginPrompt.show();
        $photoDiv.hide();
        $returnButton.hide();
        $('#login-email').val("").blur();
        $('#login-password').val("").blur();
        $('#signup-email').val("").blur();
        $('#signup-name').val("").blur();
        $('#signup-alias').val("").blur();
        $('#signup-password').val("").blur();
        $('#signup-password-2').val("").blur();
        return false;
    });

    // ensure no user session is active
    //pdd.logout();
    // start firebase auth listener only after all callbacks are in place
    pdd.start();
});
