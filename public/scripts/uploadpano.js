/**
 * Created by Nick on 4/21/2016.
 */

$(function() {
    var firebase = new Firebase('https://panorama-dd.firebaseio.com');

    /* Initialization */
    $('.error').hide();
    $('#inp-tags').tagsInput();

    /* Helper Functions */
    var showError = function(title, text) {
        $('#btn-submit').prop('disabled', false);
        var $error = $('.error');
        $error
            .html('<h3>' + title + '</h3><p>' + text + '</p>')
            .slideDown();
        setTimeout(function() {
            $error.slideUp('fast');
        }, 5000);
    };

    var clearForm = function() {
        $('#form-upload input, #form-upload textarea').val('');
        $('#btn-submit').prop('disabled', false);
    };

    /* Event Listners */
    $('#form-upload').on('submit', function(evt) {
        evt.preventDefault();

        // Disable the submit button
        $('#btn-submit').prop('disabled', true);

        // Validate that everything is filled out
        if ($('#inp-pano').val() == '' || $('#inp-title').val() == '') {
            showError('Required Field Missing',
                'Please fill out all required fields.');
            return;
        }

        // Check that we are logged in
        var authData = firebase.getAuth();
        if (!authData) {
            showError('Not Logged In', 'You must be logged in to post!');
            return;
        }

        var file = $('#inp-pano')[0].files[0];
        var okFileTypes = ['image/jpeg', 'image/png', 'image/tiff'];
        if ($.inArray(file.type, okFileTypes) < 0) {
            showError('Unrecognized Image Format',
                'Please upload your panorama in JPEG, PNG, or TIFF format.');
            $('#inp-pano').val('');
            return;
        }

        var tags = [];
        var $tagInput = $('#inp-tags');
        if ($tagInput.val() !== '') {
            var hash = {};
            tags = $tagInput.val()
                .split(',')             // split into array of tags
                .filter(function(tag) { // remove duplicates and non-strings
                    if (typeof tag === 'string') {
                        return (hash.hasOwnProperty()) ? false : (hash[tag] = true);
                    }
                    return false;
                });
        }

        var formdata = new FormData($('#form-upload')[0]);
        var popup = new Popup('<h2 style="display: inline-block">Uploading Panorama...</h2>'+
            '<img style="width: 64px;float:right;" src="images/loading.gif" />');
        $.ajax({
            url: '/upload',
            type: 'POST',
            data: formdata,
            //async: false,
            success: function(data) {
                console.log('success!');
                var info = {
                    title: $('#inp-title').val(),
                    description: $('#inp-desc').val() || '',
                    creator: authData.uid,
                    date: $('#inp-taken').val() || '',
                    favorites: 0,
                    likes: 0,
                    tags: tags,
                    'aws-url': data.path
                };

                // Push to the firebase pano section
                var databaseinfo = firebase.child('images').push(info);

                // Push to the user's gallery
                firebase.child('users')
                    .child(authData.uid)
                    .child('gallery')
                    .push(databaseinfo.key());
                popup.setContent('<h2>Success!</h2>');
                clearForm();
                setTimeout(function() {
                    window.location.href = '/pano/' + databaseinfo.key();
                })
            },
            error: function(err) {
                console.log('Error: ', err);
                popup.close();
                clearForm();
            },
            cache: false,
            contentType: false,
            processData: false
        });
        popup.show();
    });
});
