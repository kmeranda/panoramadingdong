
function Popup(content) {
    this.content = content;
    this.show = function() {
        if (!this.element) {
            this.element = $('<div class="popup">' + this.content + '</div>');
            $('body').append(this.element);
        }

        this.element.show();
        $('.popup-shadow').show();
    }
    this.setContent = function(content) {
        this.content = content;
        if (this.element) {
            this.element.html(content);
        }
    }
    this.close = function() {
        this.element.hide();
        $('.popup-shadow').hide();
    }
}

function thumbs(firebase, $parent, filterArray) {
    $parent.append($('<ul></ul>'));
    $parent = $parent.children('ul');
    firebase.on('child_added', function(snapshot) {
        var usersRef = new Firebase('https://panorama-dd.firebaseio.com/users');
        var pano = snapshot.val();
        if (filterArray && filterArray.indexOf(snapshot.key()) < 0) {
            return;
        }
        usersRef.child(pano.creator).once('value', function (userSnapshot) {
            var user = userSnapshot.val();
            var $newPano = $('<li id="thumb-' + snapshot.key() +
                '"><a href="/pano/' + snapshot.key() + '"><img src="' + pano['aws-url'].replace('pano.dzi', 'thumb.jpg') + '" />' +
                '<div class="overlay"><b>' + pano.title + '</b><p>by ' + user.name + '</p>'+
                '<img src="/images/thumbup.svg" class="inline-icon" /> ' + pano.likes + '  |  ' +
                '<img src="/images/star.svg" class="inline-icon" /> ' + pano.favorites +
                '</div></a></li>');
            $newPano.data(pano);
            $newPano.find('img').on('mouseenter', function(evt) {
                $(evt.delegateTarget).siblings('.overlay').fadeIn('fast');
                evt.stopPropagation();
            });
            $newPano.find('.overlay').on('mouseleave', function(evt) {
                $(evt.delegateTarget).fadeOut('fast');
                evt.stopPropagation();
            });
            $parent.append($newPano);
        });
    });
    firebase.on('child_removed', function(snapshot) {
        if (filterArray && filterArray.indexOf(snapshot.key()) < 0) {
            return;
        }
        $('#thumb-' + snapshot.key()).remove();
    });
    firebase.on('child_changed', function(snapshot) {
        if (filterArray && filterArray.indexOf(snapshot.key()) < 0) {
            return;
        }
        var pano =  snapshot.val();
        var $itemToChange = $('#thumb-' + snapshot.key());
        $itemToChange.html('<img src="' + pano['aws-url'] + '" />' +
            '<div class="overlay"><b>' + pano.title + '</b><p>' + user.name + '</p></div>');
        $itemToChange.data(pano);
    });
}
