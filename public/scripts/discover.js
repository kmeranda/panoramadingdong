/**
 * Created by Nick on 4/15/2016.
 */

$(function () {
    var flavorTextList = ['exciting', 'new', 'curious', 'inspiring', 'unexpected'];
    var flavorText = flavorTextList[Math.floor(Math.random() * flavorTextList.length)];

    $('#flavor-text').html(flavorText + '...');
});