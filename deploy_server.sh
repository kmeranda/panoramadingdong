#!/bin/sh

# this script kills processes using the 3000 port and runs the server 
kill -9 $(lsof -i:3000 -t) 2> /dev/null
forever start ~/panoramadingdong/server.js &
