/**
 * Created by Nick on 4/11/2016.
 *
 * Main entry point for the express server.
 */

var express = require('express');
var formidable = require('formidable');
var Firebase = require('firebase');
var handlebars = require('express-handlebars')
    .create({defaultLayout: 'main'});
var s3 = require('s3');
var cors = require('cors');
var fs = require('fs-extra');
var path = require('path');
var async = require('async');
var cp = require('child_process');
if (process.platform !== 'win32') {
    var sharp = require('sharp');
}

var app = express();
app.use(express.static(__dirname + '/public'));
app.set('port', process.env.PORT || 3000);
app.engine('handlebars', handlebars.engine);
app.set('view engine', 'handlebars');

/* Application views (get handlers) */

app.get('/', function (req, res) {
    res.set('Content-Type', 'text/html');
    res.render('index', {title: 'Panoramadingdong', styles: ['/styles/index.css'] });
});

/* Use this as a template for adding new endpoints
app.get('/view', function (req, res) {
    res.set('Content-Type', 'text/html');

    // 'viewer' is the name of the .handlebars file (this would load the file 'views/viewer.handlebars')
    res.render('viewer', {title: 'Panorama Viewer'});
});
*/

app.get('/upload', function (req, res) {
    res.set('Content-Type', 'text/html');
    res.render('form-upload', {
        title: 'Upload Panorama',
        scripts: ['/scripts/uploadpano.js', '/scripts/jquery.tagsinput.js'],
        styles: ['/styles/jquery.tagsinput.css']
    });
});

app.get('/pano/:pid', function (req, res) {
    res.set('Content-Type', 'text/html');
    res.render('viewer', {
        title: 'Panorama Viewer',
        scripts: ['/scripts/viewer.js'],
        styles: ['/styles/viewer.css'],
        pano: req.params.pid
     });
});

app.get('/users/:uid', function (req, res) {
    res.set('Content-Type', 'text/html');
    res.render('profile', {
        title: 'User Account',
        scripts: ['/scripts/profile.js'],
        styles: ['/styles/profile.css'],
        user: req.params.uid
    });
});

app.get('/discover', function (req, res) {
    res.set('Content-Type', 'text/html');
    res.render('discover', {title: 'Explore Panoramas', scripts: ['/scripts/discover.js', '/scripts/search.js'], styles: ['/styles/discover.css']});
});

app.get('/browse', function (req, res) {
    res.set('Content-Type', 'text/html');
    res.render('browse', {title: 'Panorama Browser', scripts: ['/scripts/browse.js']});
});

/* Server interfaces (post handlers) */
app.post('/upload', cors(), function (req, res) {
    var status = {success: false, path: '', message: ''};
    var form = new formidable.IncomingForm();
    form.keepExensions = true;
    form.parse(req);

    res.set('Content-Type', 'text/json');

    form.on('file', function (name, file) {
        // Create a wrapper folder for the result
        fs.mkdirSync(file.path + '_dz/');
        //console.log(file.path);

        async.parallel([function(done) { // split image to tiles
            sharp(file.path)
                .tile()
                .toFile(file.path + '_dz/pano.dzi', done);
        }, function(done) { // generate thumbnail image
            sharp(file.path)
                .resize(256, 256)
                .crop(sharp.gravity.center)
                .toFile(file.path + '_dz/thumb.jpg', done);
        }], function(err, results) {
            var s3client = s3.createClient({
                s3Options: {
                    accessKeyId: 'AKIAJPDEJL72SWE5MQBA',
                    secretAccessKey: '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',
                }
            });

            var uploader = s3client.uploadDir({
                localDir: file.path + '_dz/',
                s3Params: {
                    Bucket: 'naiello',
                    Prefix: 'pano/' + path.basename(file.path + '_dz/'),
                    ACL: 'public-read'
                }
            });

            uploader.on('error', function(err) {
    		console.log(err);
                status.message = err;
                res.json(status);
                fs.remove(file.path + '_dz/');
                fs.remove(file.path);
            });

            uploader.on('end', function () {
                status.success = true;
                status.path = 'https://s3.amazonaws.com/naiello/pano' +
                    file.path.replace('/tmp', '') + '_dz/pano.dzi';
                res.json(status);
                fs.remove(file.path + '_dz/');
                fs.remove(file.path);
            });
        });
    });
});

app.listen(app.get('port'), function () {
   console.log('Express started on port ' + app.get('port'));
});
