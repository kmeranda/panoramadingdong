
var Firebase = require('firebase');
var elasticsearch = require('elasticsearch');

var client = new elasticsearch.Client({host: 'localhost:9200', port: 9200});
var firebase = new Firebase('https://panorama-dd.firebaseio.com/images');

firebase.on('child_added', createOrUpdateIndex);
firebase.on('child_changed', createOrUpdateIndex);
firebase.on('child_removed', removeIndex);

if (!client.indices.exists({index:'imageindex'})) {
    client.indices.create({index:'imageindex'});
}

function createOrUpdateIndex(snapshot) {
    client.index({
        index: 'imageindex',
        type: 'image',
        id: snapshot.key(),
        body: snapshot.val(),
    }, function (error, response) {
        if (error) {
            console.log(error);
        }
    });
}

function removeIndex(snapshot) {
    client.delete({
        index: 'imageindex',
        type: 'image',
        id: snapshot.key()
    });
}

var searchRef = new Firebase('https://panorama-dd.firebaseio.com/search');

searchRef.child('request').on('child_added', function (snapshot) {
    snapshot.ref().remove();
    queryString = '';
    tok = snapshot.val().split(' ');
    for (var i = 0; i < tok.length; i++) {
        if (i > 0) {
            queryString += ' OR ';
        }
        queryString += 'tags:'+tok[i]+' OR ' +
            'title:'+tok[i]+' OR ' +
            'description:'+tok[i];
    }
    client.search({
        index: 'imageindex',
        body: {
            query: {
                query_string: {
                    default_field: 'title',
                    query: queryString
                }
            }
        }
    }, function (error, response) {
        searchRef.child('response/'+snapshot.key()).set(response);
    });
});
